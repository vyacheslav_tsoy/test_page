
$(document).ready(function(){

	if ($("#range-slider").length){
		$( "#range-slider" ).slider({
      range: "min",
      value: 4,
      min: 0,
      max: 4
    });
	}

	if ($(".form-field__inp").length){
		
		$(".form-field__inp").on('blur', function(){
			var currentInp = $(this);

			if (currentInp.val()){
				currentInp.addClass('active')
			}
			else{
				currentInp.removeClass('active');
			}

		})

		$(".form-field__inp").each(function() {
			var currentInp = $(this);
			
			if (currentInp.val()){
				currentInp.addClass('active')
			}
			else{
				currentInp.removeClass('active');
			}

		});

	}


	if ($('select').length){
		$('select').each(function(){
		  var $this = $(this), 
		  		numberOfOptions = $(this).children('option').length;

		  $this.addClass('select-hidden'); 
		  $this.wrap('<div class="select"></div>');
		  $this.after('<div class="select-styled"></div>');

		  var $styledSelect = $this.next('div.select-styled');

		  if ($this.find('option:selected').length){
		  	$styledSelect.text($this.children('option:selected').text());

		  	if ($this.find('option:selected').index() > 0){
		  		$this.parent().addClass('selected');
		  	}

		  }
		  else{
		  	$styledSelect.text($this.children('option').eq(0).text());
		  	$this.parent().removeClass('selected');
		  }

		  var $list = $('<ul />', {
		      'class': 'select-options'
		  }).insertAfter($styledSelect);

		  for (var i = 0; i < numberOfOptions; i++) {
		      $('<li />', {
		          text: $this.children('option').eq(i).text(),
		          rel: $this.children('option').eq(i).val()
		      }).appendTo($list);
		  }

		  var $listItems = $list.children('li');

		  $styledSelect.click(function(e) {
		      e.stopPropagation();
		      $('div.select-styled.active').not(this).each(function(){
		          $(this).removeClass('active').next('ul.select-options').hide();
		      });
		      $(this).toggleClass('active').next('ul.select-options').toggle();
		  });

		  $listItems.click(function(e) {
		      e.stopPropagation();
		      $styledSelect.text($(this).text()).removeClass('active');
		      $this.val($(this).attr('rel'));
		      if ($(this).index() >= 1){
		      	$this.parent().addClass('selected');
		      }
		      else{
		      	$this.parent().removeClass('selected');
		      }
		      $list.hide();
		      //console.log($this.val());
		  });

		  $(document).click(function() {
		      $styledSelect.removeClass('active');
		      $list.hide();
		  });

		});
	}
	
	// responsive navigation toggle button
	$("#js-toggle-btn").on('click', function(){
	  	$("body").toggleClass('navTrue');
	})


})